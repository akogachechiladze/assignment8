package com.example.assignment8_1.ui.dashboard

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.assignment8.network.NetworkClient
import com.example.assignment8_1.DataRecycler
import com.example.assignment8_1.ResultOf
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

class DashboardViewModel : ViewModel() {

    private val _data = MutableLiveData<ResultOf<MutableList<DataRecycler>>>()
    val data: LiveData<ResultOf<MutableList<DataRecycler>>> get() = _data


    fun load() {
        viewModelScope.launch {
            try {
                val response = NetworkClient.apiClient.getActiveCourse()
                val result = response.body()

                if (response.isSuccessful && result != null){
                    Log.d("result", "load:${result as MutableList<DataRecycler>?}")
                    _data.postValue (ResultOf.Success(response as MutableList<DataRecycler>))
//                    Log.d("result", "load:${result}")
//                    Log.d("result", "observes: ${_loading.value}")



                }

            } catch (ioe: IOException) {
                _data.postValue(ResultOf.Failure("[IO] error please retry", ioe))
            } catch (he: HttpException) {
                _data.postValue(ResultOf.Failure("[HTTP] error please retry", he))
            }


        }


    }
}