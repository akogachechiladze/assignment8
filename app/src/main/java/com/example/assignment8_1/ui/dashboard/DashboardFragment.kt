package com.example.assignment8_1.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assignment8.adapters.FirstRecyclerAdapter
import com.example.assignment8_1.R
import com.example.assignment8_1.ResultOf
import com.example.assignment8_1.databinding.FragmentDashboardBinding

class DashboardFragment : Fragment() {

    private var _binding: FragmentDashboardBinding? = null
    val binding get() = _binding!!

    private lateinit var adapter: FirstRecyclerAdapter

    private val viewModel: DashboardViewModel by viewModels()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        observes()
        viewModel.load()
    }

    private fun initRecyclerView() {
        binding.mainRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapter = FirstRecyclerAdapter()
        binding.mainRecyclerView.adapter = adapter
    }

    private fun observes() {

        viewModel.data.observe(viewLifecycleOwner, {result ->

            when (result) {

                is ResultOf.Success -> {
                    adapter.setData(result.value)
//                    Log.d("result", "observes: ${result}")

                }

                is ResultOf.Failure -> {
                    Toast.makeText(
                        requireContext(), result.message
                            ?: "Unknown error", Toast.LENGTH_SHORT
                    ).show()
//                    Log.d("result", "observes: ${result}")
                }
            }
        })

    }
}