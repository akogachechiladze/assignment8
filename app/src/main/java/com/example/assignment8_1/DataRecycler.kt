package com.example.assignment8_1


import com.google.gson.annotations.SerializedName

data class DataRecycler(
    @SerializedName("active_courses")
    val activeCourses: List<ActiveCourse>?,
    @SerializedName("new_courses")
    val newCourses: List<NewCourse>?
){data class NewCourse(
    @SerializedName("duration")
    val duration: String?,
    @SerializedName("icon_type")
    val iconType: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("main_color")
    val mainColor: String?,
    @SerializedName("question")
    val question: String?,
    @SerializedName("title")
    val title: String?
)data class ActiveCourse(
    @SerializedName("background_color_percent")
    val backgroundColorPercent: String?,
    @SerializedName("booking_time")
    val bookingTime: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("image")
    val image: String?,
    @SerializedName("main_color")
    val mainColor: String?,
    @SerializedName("play_button_color_percent")
    val playButtonColorPercent: String?,
    @SerializedName("progress")
    val progress: String?,
    @SerializedName("title")
    val title: String?
)}