package com.example.assignment8.network


import com.example.assignment8_1.DataRecycler
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("4167a598-b68c-420f-b6e1-fef68b89a10d")
    suspend fun getNewCourse(): Response<DataRecycler.NewCourse>

    @GET("4167a598-b68c-420f-b6e1-fef68b89a10d")
    suspend fun getActiveCourse(): Response<DataRecycler.ActiveCourse>


}