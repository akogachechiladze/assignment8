package com.example.assignment8.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment8.extensions.setImage
import com.example.assignment8_1.DataRecycler
import com.example.assignment8_1.databinding.ItemsFirstRecyclerBinding
import com.example.assignment8_1.databinding.SecondRecyclerviewLayoutBinding

class FirstRecyclerAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items1 = mutableListOf<DataRecycler.ActiveCourse>()

    companion object {
        private const val FIRST_ITEM = 1
        private const val SECOND_ITEM = 2
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == FIRST_ITEM) {
            ViewHolder1(ItemsFirstRecyclerBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        } else {
            ViewHolder2(SecondRecyclerviewLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder1) {
            holder.onBind()
        }else if (holder is ViewHolder2) {
            holder.onBind()
        }
    }

    override fun getItemCount(): Int {
        TODO("Not yet implemented")
    }


    inner class ViewHolder1(private val binding: ItemsFirstRecyclerBinding): RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: DataRecycler.ActiveCourse
        @SuppressLint("SetTextI18n")
        fun onBind() {
            model = items1[adapterPosition]
            binding.IV1.setImage(model.image)
            binding.TV1.text = "Booked for "+model.bookingTime
            binding.TV2.text =  "Booked for "+model.bookingTime
            binding.cardViewFirstRecycler.setCardBackgroundColor(model.backgroundColorPercent!!.toInt())

        }
    }


    inner class ViewHolder2(private val binding: SecondRecyclerviewLayoutBinding): RecyclerView.ViewHolder(binding.root){
        private lateinit var adapter: SecondRecyclerAdapter
        fun onBind() {
            binding.secondRecyclerView.layoutManager = LinearLayoutManager(itemView.context)
            adapter = SecondRecyclerAdapter()
            binding.secondRecyclerView.adapter = adapter
        }

    }

    fun setData(news: MutableList<DataRecycler>) {
        items1.clear()
        items1.addAll(items1)
        notifyDataSetChanged()

    }

}