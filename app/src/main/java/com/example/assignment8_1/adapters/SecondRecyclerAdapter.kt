package com.example.assignment8.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment8_1.DataRecycler
import com.example.assignment8_1.R
import com.example.assignment8_1.databinding.ItemsSecondRecyclerBinding

class SecondRecyclerAdapter: RecyclerView.Adapter<SecondRecyclerAdapter.ViewHolder>() {

    private val items = mutableListOf<DataRecycler.NewCourse>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ItemsSecondRecyclerBinding.inflate(
            LayoutInflater.from(parent.context),parent,false
        )
    )

    override fun onBindViewHolder(holder: SecondRecyclerAdapter.ViewHolder, position: Int) {
        TODO("Not yet implemented")
    }

    override fun getItemCount() = items.size


    inner class ViewHolder(val binding: ItemsSecondRecyclerBinding):RecyclerView.ViewHolder(binding.root){
        private lateinit var model: DataRecycler.NewCourse
        fun onBind() {
            model = items[adapterPosition]
            if (model.iconType == "settings")
                binding.introduceIV.setImageResource(R.drawable.ic_settings)
            else if(model.iconType == "card")
                binding.introduceIV.setImageResource(R.drawable.ic_card)
            else
                binding.introduceIV.setImageResource(R.mipmap.ic_launcher)

            binding.introduceTV.text = model.title
            binding.whatIsIt.text = model.question
//            binding.time.setText(((model.duration?.toDouble() ?: 0) % 60000))
            binding.cardViewSecondRecycler.setCardBackgroundColor(model.mainColor!!.toInt())
        }
    }

}